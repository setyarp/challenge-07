# CH 7 - Binar Car Rental With React JS

This project was made to fulfill the challenge chapter 7 with the criteria of making a react application, implementing redux, react router, search car data feature with filters, implementing Google OAuth , and styling with bootstrap.

## The tasks contained in the challenge include:

### Landing Pages

- As a user, I can search the list of cars I want to rent
  via the landing page:
- GIVEN: I'm on the landing page
- WHEN: When I press the Start Car Rental button
- THEN: I will be redirected to the Search Cars page

### Car Search Page

- As a user, I can search the list of cars I want to rent
  via the Search Cars page.
- GIVEN: I'm on the car search page
- WHEN: When I fill out the form and press the Search button
  Car
- THEN: I'll be shown a list of cars that match
  given filters.

## Getting Started

### 1. Install Dependency

```bash
# NPM user
npm install
```

### 2. Config .env Variable

You can look at .env-example to assign .env variable for the client Id of Google Auth

## Run Project

```
npm start
```
