import logo from "./logo.svg";

import "./App.css";
import Navbar from "./components/Header/Navbar";
import Hero from "./components/Header/Hero";
import OurService from "./components/Homepage/OurService";
import WhyUs from "./components/Homepage/WhyUs";
import Testimonials from "./components/Homepage/Testimonials";
import Banner from "./components/Homepage/Banner";
import Faq from "./components/Homepage/Faq";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />
      <OurService />
      <WhyUs />
      <Testimonials />
      <Banner />
      <Faq />
      <Footer />
    </div>
  );
}

export default App;
