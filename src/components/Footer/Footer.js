import React from "react";

import classes from "./css/Footer.module.css";

import facebook from "./img/icons/icon_facebook.svg";
import instagram from "./img/icons/icon_instagram.svg";
import twitter from "./img/icons/icon_twitter.svg";
import mail from "./img/icons/icon_mail.svg";
import twitch from "./img/icons/icon_twitch.svg";
import logo from "./img/logo.png";

const Footer = () => {
  return (
    <div id="footer" className={classes.footer}>
      <div class="container px-lg-5 mt-5">
        <div class="row">
          <div class="col-lg-3">
            <p class="inContent">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            <p class="inContent">binarcarrental@gmail.com</p>
            <p class="inContent">081-233-334-808</p>
          </div>
          <div class="col-lg-3">
            <p>
              <a className={classes.footerLink} href="#our-service">
                <p>Our Services</p>
              </a>
            </p>
            <p>
              <a className={classes.footerLink} href="#why-us">
                <p>Why Us</p>
              </a>
            </p>
            <p>
              <a className={classes.footerLink} href="#testimonial">
                <p>Testimonial</p>
              </a>
            </p>
            <p>
              <a className={classes.footerLink} href="#faq">
                <p>FAQ</p>
              </a>
            </p>
          </div>
          <div class="col-lg-3">
            <p class="inContent">Connect with us</p>
            <div class="social-media">
              <a href="#">
                <img class="me-3" src={facebook} alt="facebook" />
              </a>
              <a href="#">
                <img class="me-3" src={instagram} alt="instagram" />
              </a>
              <a href="#">
                <img class="me-3" src={twitter} alt="twitter" />
              </a>
              <a href="#">
                <img class="me-3" src={mail} alt="mail" />
              </a>
              <a href="#">
                <img class="me-3" src={twitch} alt="twitch" />
              </a>
            </div>
          </div>
          <div class="col-lg-3">
            <p class="inContent">Copyright Binar 2022</p>
            <img src={logo} />
          </div>
          <div>&nbsp;</div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
