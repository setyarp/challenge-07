import React, { useEffect, useState } from "react";
import Button from "../UI/Button";
import GoogleLogin from "react-google-login";
import { gapi } from "gapi-script";
import ErrorModal from "../UI/ErrorModal";
import car from "./img/img_car.png";
import classes from "./css/Hero.module.css";

const Hero = () => {
  const [token, setToken] = useState(false);
  const [errorModal, setErrorModal] = useState(false);

  const responseGoogle = (response) => {
    setToken(true);
    window.localStorage.setItem("accessToken", response.accessToken);
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID,
        scope: "",
      });
    }

    gapi.load("client:auth2", start);
  }, []);

  return (
    <>
      <header className={classes.hero}>
        <div class="container">
          <div class="row">
            <div class="col-md-6 mt-5">
              <h1 className={classes.heroText}>Sewa & Rental Mobil Terbaik di kawasan Purwokerto</h1>
              <br />
              <div class="tron-lead">
                <p class="inContent">
                  Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas <br />
                  terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu <br />
                  untuk sewa mobil selama 24 jam.
                </p>
                {token ? (
                  <a href="/cars" className="me-3">
                    {window.location.pathname !== "/cars" && <Button type="submit">Mulai Sewa Mobil</Button>}
                  </a>
                ) : (
                  <div className="me-3 d-inline">
                    <Button onClick={() => setErrorModal(true)}>Mulai Sewa Mobil</Button>{" "}
                  </div>
                )}

                {window.location.pathname !== "/cars" && (
                  <GoogleLogin clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID} buttonText="Google Login" onSuccess={responseGoogle} onFailure={responseGoogle} cookiePolicy={"single_host_origin"} className="ms-4" />
                )}
              </div>
            </div>
            <div class="col-md-6">
              <div className={classes.imgHero}>
                <img src={car} />
              </div>
            </div>
          </div>
        </div>
        <ErrorModal show={errorModal} title="Membutuhkan Authentikasi" message="Mohon Login Terlebih Dahulu Menggunakan Akun Google Anda" onHide={() => setErrorModal(false)} />
      </header>
    </>
  );
};

export default Hero;
