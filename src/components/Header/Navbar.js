import React from "react";

import classes from "./css/Navbar.module.css";
import logo from "./img/logo.png";
// import Button from "../UI/ButtonLogin";

const Navbar = (props) => {
  return (
    <div className={classes.navbar}>
      <nav class="navbar navbar-expand-lg" aria-label="Offcanvas navbar large">
        <div class="container px-lg-5 my-2">
          <a class="navbar-brand ms-lg-4" href="#">
            <img src={logo} alt="logo" class="img-fluid" />
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNav" aria-controls="offcanvasNav">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNav" aria-labelledby="offcanvasNavLabel">
            <div class="offcanvas-header">
              <h5 class="offcanvas-title" id="offcanvasNavLabel">
                BCR
              </h5>
              <button type="button" class="btn-close btn-close-black" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <ul class="navbar-nav ms-auto">
                <li class={classes.navItem}>
                  <a class={classes.navLink} aria-current="page" href="#our-service">
                    Our Services
                  </a>
                </li>
                <li class={classes.navItem}>
                  <a class={classes.navLink} href="#why-us">
                    Why Us
                  </a>
                </li>
                <li class={classes.navItem}>
                  <a class={classes.navLink} href="#testimonial">
                    Testimonial
                  </a>
                </li>
                <li class={classes.navItem}>
                  <a class={classes.navLink} href="#faq">
                    FAQ
                  </a>
                </li>
                <li class="nav-item ms-2 btn btn-success">Register</li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
