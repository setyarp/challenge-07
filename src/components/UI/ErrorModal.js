import React from "react";
import { MDBBtn, MDBModal, MDBModalDialog, MDBModalContent, MDBModalHeader, MDBModalTitle, MDBModalBody, MDBModalFooter } from "mdb-react-ui-kit";

const ErrorModal = (props) => {
  return (
    <MDBModal {...props} tabIndex="-1">
      <MDBModalDialog>
        <MDBModalContent>
          <MDBModalHeader closeButton>
            <MDBModalTitle>{props.title}</MDBModalTitle>
          </MDBModalHeader>
          <MDBModalBody>
            <h5>{props.message}</h5>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="danger" onClick={props.onHide}>
              Close
            </MDBBtn>
          </MDBModalFooter>
        </MDBModalContent>
      </MDBModalDialog>
    </MDBModal>
  );
};

export default ErrorModal;
