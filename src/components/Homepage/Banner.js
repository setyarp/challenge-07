import React from "react";
import Button from "../UI/Button";

import classes from "./css/Banner.module.css";

const Banner = () => {
  return (
    <div className={classes.banner}>
      <div class="container px-lg-5 mt-5 mw-100 mh-100 text-center d-flex align-items-center">
        <div className={classes.box}>
          <h1 className={classes.text}>Sewa Mobil di Purwokerto Sekarang</h1>
          <br />
          <p class="inContent">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br />
            tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <br />
          <a href="/cars">
            <Button>Mulai Sewa Mobil</Button>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Banner;
