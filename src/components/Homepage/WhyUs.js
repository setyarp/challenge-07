import React from "react";

import classes from "./css/WhyUs.module.css";

import iconComplete from "./img/icons/icon_complete.svg";
import iconPrice from "./img/icons/icon_price.svg";
import iconHours from "./img/icons/icon_24hrs.svg";
import iconProfesional from "./img/icons/icon_professional.svg";

const WhyUs = () => {
  return (
    <div id="why-us" className={classes.whyUs}>
      <div class="subTitle">Why Us</div>
      <br />
      <p class="in-content content-why-us">Mengapa harus pilih Binar Car Rental?</p>
      <br />
      <div class="row">
        <div class="col d-flex align-items-stretch">
          <div class="card">
            <div class="card-body mt-2">
              <img src={iconComplete} alt="" class="mb-3" />
              <h5 className={classes.cardTitle}>Mobil Lengkap</h5>
              <p class="inContent">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
          </div>
        </div>
        <div class="col d-flex align-items-stretch">
          <div class="card">
            <div class="card-body mt-2">
              <img src={iconPrice} alt="" class="mb-3" />
              <h5 className={classes.cardTitle}>Harga Murah</h5>
              <p class="inContent">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
            </div>
          </div>
        </div>
        <div class="col d-flex align-items-stretch">
          <div class="card">
            <div class="card-body mt-2">
              <img src={iconHours} alt="" class="mb-3" />
              <h5 className={classes.cardTitle}>Layanan 24 Jam</h5>
              <p class="inContent">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
            </div>
          </div>
        </div>
        <div class="col d-flex align-items-stretch">
          <div class="card">
            <div class="card-body mt-2">
              <img src={iconProfesional} alt="" class="mb-3" />
              <h5 className={classes.cardTitle}>Sopir Profesional</h5>
              <p class="inContent">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WhyUs;
