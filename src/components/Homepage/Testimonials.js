import React from "react";
import OwlCarousel from "react-owl-carousel";

import classes from "./css/Testimonials.module.css";
import rate from "./img/Rate.svg";
import photoOne from "./img/photo1.svg";
import photoTwo from "./img/photo2.svg";
import photoThree from "./img/photo3.svg";
import leftButton from "./img/icons/leftButton.svg";
import rightButton from "./img/icons/rightButton.svg";

const Testimonial = () => {
  return (
    <>
      <div className="card-item pt-5" id="testimonial">
        <div className="container px-lg-5 mt-5">
          <div className={classes.text}>
            <h2 class="subTitle">Testimonial</h2>
            <p class="inContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
          </div>
        </div>
      </div>
      <OwlCarousel
        className="owl-theme"
        loop
        autoWidth
        center
        nav
        navText={[`<img src=${leftButton}>`, `<img src=${rightButton}>`]}
        dots={false}
        responsiveClass
        responsive={{
          0: {
            items: 1,
          },
          600: {
            items: 2,
          },
          1000: {
            items: 2,
          },
        }}
      >
        <div className={classes.item}>
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className={classes.profile}>
                  <div className={classes.imgArea}>
                    <img src={photoOne} alt="Jhon" />
                  </div>
                </div>
              </div>
              <div className="col-md-10">
                <div className={classes.content}>
                  <div className={classes.rate}>
                    <img src={rate} alt="" />
                  </div>
                  <div>
                    <p class="inContent mt-5">
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="inContentBold mt-2">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.item}>
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className={classes.profile}>
                  <div className={classes.imgArea}>
                    <img src={photoTwo} alt="Jhon" />
                  </div>
                </div>
              </div>
              <div className="col-md-10">
                <div className={classes.content}>
                  <div className={classes.rate}>
                    <img src={rate} alt="" />
                  </div>
                  <div>
                    <p class="inContent mt-5">
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="inContentBold mt-2">Jenifer Lee 25, Alpen</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.item}>
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className={classes.profile}>
                  <div className={classes.imgArea}>
                    <img src={photoThree} alt="Jhon" />
                  </div>
                </div>
              </div>
              <div className="col-md-10">
                <div className={classes.content}>
                  <div className={classes.rate}>
                    <img src={rate} alt="" />
                  </div>
                  <div>
                    <p class="inContent mt-5">
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="inContentBold mt-2">George Paiman 30, Texas</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </OwlCarousel>
    </>
  );
};

export default Testimonial;
