import React from "react";

import classes from "./css/OurService.module.css";
import imgGirl from "./img/img_service.png";
import imgCheck from "./img/check.png";

const OurService = () => {
  return (
    // Start Our Service Section
    <div id="our-service" className={classes.service}>
      <div class="row justify-content-center">
        <div class="col-lg-4 me-lg-3 mb-4">
          <img src={imgGirl} alt="" />
        </div>
        <div class="col-lg-4 mt-lg-4 ms-lg-3">
          <h2 class="subTitle">Best Car Rental for any kind of trip in Purwokerto!</h2>
          <br />
          <p className={classes.inContent}>
            Sewa mobil di Purwokerto bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
          </p>
          <div className={classes.inContent}>
            <img src={imgCheck} alt="" />
            Sewa Mobil Dengan Supir di Bali 12 Jam
          </div>
          <div className={classes.inContent}>
            <img src={imgCheck} alt="" />
            Sewa Mobil Lepas Kunci di Bali 24 Jam
          </div>
          <div className={classes.inContent}>
            <img src={imgCheck} alt="" />
            Sewa Mobil Jangka Panjang Bulanan
          </div>
          <div className={classes.inContent}>
            <img src={imgCheck} alt="" />
            Gratis Antar - Jemput Mobil di Bandara
          </div>
          <div className={classes.inContent}>
            <img src={imgCheck} alt="" />
            Layanan Airport Transfer / Drop In Out
          </div>
        </div>
      </div>
    </div>
    // End Our Service Section

    // Start Why Us Section

    // End Why Us Section
  );
};

export default OurService;
