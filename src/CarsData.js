import React, { useState } from "react";
import Navbar from "./components/Header/Navbar";
import Hero from "./components/Header/Hero";
import SearchCars from "./components/Search/Search";
import Card from "./components/Search/Card";
import Footer from "./components/Footer/Footer";

const CarsData = (props) => {
  const [input, setinput] = useState("");
  const filter = (props) => {
    return setinput(props);
  };

  return (
    <>
      <Navbar />
      <Hero />
      <SearchCars filter={filter} />
      <Card input={input} />
      <Footer />
    </>
  );
};

export default CarsData;
